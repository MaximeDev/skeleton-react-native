import React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';

const FirstPageScreen = ({
    navigation,
}) => (
        <View style={styles.container}>
            <Text>FirstPageScreen</Text>
            <Button onPress={() => {
                navigation.navigate("SecondePageScreen");
            }}
                title="Aller sur la deuxième page" />
        </View>
    );

export default FirstPageScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
