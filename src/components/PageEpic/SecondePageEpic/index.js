import React from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';

const SecondePageScreen = ({
    navigation,
}) => (
        <View style={styles.container}>
            <Text>SecondePageScreen</Text>
            <Button onPress={() => {
                navigation.goBack();
            }}
                title="Revenir" />

        </View>
    );

export default SecondePageScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
