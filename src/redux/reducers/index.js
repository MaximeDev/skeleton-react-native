import { combineReducers } from 'redux';

import { sampleReducers } from './sampleReducer'

export default combineReducers({
    sampleReducers,
});