import React from 'react';
import { StyleSheet, SafeAreaView, Platform } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import store from './src/redux/store';
import FirstPageScreen from './src/components/PageEpic/FirstPageEpic'
import SecondePageScreen from './src/components/PageEpic/SecondePageEpic'

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer style={styles.container}>
        <Stack.Navigator>
          <Stack.Screen name="FirstPageScreen" component={FirstPageScreen} />
          <Stack.Screen name="SecondePageScreen" component={SecondePageScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === "android" ? 25 : 0,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
