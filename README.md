# Description

squelette d'application react-native

## Vérification

Avoir Nodejs et npm d'installé 

https://nodejs.org/fr/download/

```bash
npm -v
```

Avoir expo-cli d'installé 

https://expo.io/tools
ex : pour installer
```bash
npm install expo-cli --global
```

## Installation des dépendances

ex : npm
```bash
npm install
```
ou 

ex : yarn
```bash
yarn install
```

puis faire

```bash
expo start
```

ex: affichage sur telephone
```bash
expo start --tunnel
```
Si problème de tunnel, installé une version antérieur de expo-cli
```bash
npm uninstall -g expo-cli
npm install -g expo-cli@3.21.3
```


## Installation from scratch

ex : npm
```bash
npm i -g expo-cli
expo init monProjet
cd monProjet
npm install redux react-redux redux-thunk redux-persist redux-form
npm install 
npm install @react-navigation/native @react-navigation/stack @react-navigation/bottom-tabs
npm install react-native-paper
expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
expo start
```

ex : yarn
```bash
yarn add global expo-cli
expo init monProjet
cd monProjet
yarn add redux react-redux redux-thunk redux-persist redux-form 
yarn add axios react-native-router-flux
yarn add @react-navigation/native @react-navigation/stack @react-navigation/bottom-tabs 
yarn add react-native-paper
expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
expo start
```



| Dépendance      | Description      |
| --------------- | ---------------- |
|@react-native-community/masked-view| 0.1.6|
|@react-navigation/bottom-tabs|5.5.2|
|@react-navigation/native| 5.5.0|
|@react-navigation/stack| 5.4.1|
|axios| 0.19.2|
|expo| 37.0.3|
|react| 16.9.0|
|react-dom| 16.9.0|
|react-native| https://github.com/expo/react-native/archive/sdk-37.0.1.tar.gz|
|react-native-gesture-handler| 1.6.0|
|react-native-paper|3.10.1|
|react-native-reanimate| 1.7.0|
|react-native-safe-area-context| 0.7.3|
|react-native-screens| 2.2.0|
|react-native-web| 0.11.7|
|react-redux| 7.2.0|
|redux| 4.0.5|
|redux-form|8.3.6|
|redux-persist|6.0.0|
|redux-thunk|2.3.0|